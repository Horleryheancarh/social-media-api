import { z }  from 'zod';

export const PostCreateSchema = z.object({
  content: z.string().nonempty(),
  imageUrl: z.string().url().nonempty(),
}).transform((data) => {
  if (!data.content && !data.imageUrl) {
    throw new Error('At least one of content and imageUrl must not be empty');
  }

  return data;
})

export const PostUpdateSchema = z.object({
  content: z.string().optional(),
  imageUrl: z.string().url().optional(),
})

export const IdSchema = z.object({
  id: z.string().nonempty(),
})
