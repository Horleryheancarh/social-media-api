import { z }  from 'zod';

export const CommentCreateSchema = z.object({
  content: z.string().nonempty(),
  imageUrl: z.string().url().nonempty(),
}).transform((data) => {
  if (!data.content && !data.imageUrl) {
    throw new Error('At least one of content and imageUrl must not be empty');
  }

  return data;
})

export const CommentUpdateSchema = z.object({
  content: z.string().optional(),
  imageUrl: z.string().url().optional(),
})

export const PostIdSchema = z.object({
  postId: z.string().nonempty(),
})

export const CommentIdSchema = z.object({
  postId: z.string().nonempty(),
})