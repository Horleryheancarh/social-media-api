import { z } from 'zod';

export const UserCreateSchema = z.object({
  firstname: z.string(),
  lastname: z.string(),
  middlename: z.string().optional(),
  username: z.string(),
  email: z.string().email(),
  phone: z.string().regex(/^\+(?:[0-9] ?){6,14}[0-9]$/),
  password: z.string(),
})

export const UserLoginSchema = z.object({
  identifier: z.string(),
  password: z.string(),
})
