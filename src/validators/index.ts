import { NextFunction, Request, Response } from 'express';
import { AnyZodObject, ZodError, ZodIssue } from 'zod';
import { BadRequestError } from '../utils';

const mapZodIssue = (issue: ZodIssue): any => {
  return {
    message: issue.message,
    path: issue.path.join('.'),
  };
};

const mapZodErrorToValidationError = (error: ZodError): string => {
    const issues = error.issues.map(mapZodIssue);
  const issuesCount = issues.length;
  const issuesMessage = issues
    .map(
      (issue, index) => `Issue ${index + 1}: ${issue.message} in ${issue.path}`
    )
    .join(', ');

  return `Validation Error: "issues": ${issuesCount}, ${issuesMessage}`;
};

export const bodyValidationMiddleware = (schema: AnyZodObject) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await schema.parseAsync(req.body);

      next();
    } catch (error) {
      if (error instanceof ZodError) {
        const validationError = mapZodErrorToValidationError(error);
        next(new BadRequestError(validationError));
      } else {
        next(error);
      }
    }
  };
};

export const paramValidationMiddleware = (schema: AnyZodObject) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await schema.parseAsync(req.params);

      next();
    } catch (error) {
      if (error instanceof ZodError) {
        const validationError = mapZodErrorToValidationError(error);
        next(new BadRequestError(validationError));
      } else {
        next(error);
      }
    }
  };
};
