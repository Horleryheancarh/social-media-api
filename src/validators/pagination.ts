import { z } from 'zod';

export const PaginationSchema = z.object({
  page: z.number().optional(),
  limit: z.number().optional()
})
