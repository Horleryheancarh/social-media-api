import { createClient } from 'redis';
import { REDIS_URL } from '../config';

const client = createClient({url: REDIS_URL});

(async () => await client.connect())();

interface ICacheArg {
  key: string;
  ttl: number;
  value: any;
}

export const setCache = async (arg: ICacheArg) => {
  return await client.setEx(arg.key, arg.ttl, arg.value);
};

export const getCache = async (key: string) => {
  return await client.get(key);
};

export const delCache = async (key: string) => {
  return await client.del(key);
};