abstract class CustomError extends Error {
  abstract statusCode: number;
  constructor(message: string) {
    super(message);
  }
}

export class BadRequestError extends CustomError {
  statusCode: number;
  constructor(message: string) {
    super(message);
    this.statusCode = 400;
  }
}

export class NotFoundError extends CustomError {
  statusCode: number;
  constructor(message: string) {
    super(message);
    this.statusCode = 404;
  }
}

export class ForbiddenError extends CustomError {
  statusCode: number;
  constructor(message: string) {
    super(message);
    this.statusCode = 403;
  }
}

export class NotAcceptableError extends CustomError {
  statusCode: number;
  constructor(message: string) {
    super(message);
    this.statusCode = 409;
  }
}


export class UnauthorizedError extends CustomError {
  statusCode: number;
  constructor(message: string) {
    super(message);
    this.statusCode = 401;
  }
}

export class ServerError extends CustomError {
  statusCode: number;
  constructor(message: string) {
    super(message);
    this.statusCode = 500;
  }
}
