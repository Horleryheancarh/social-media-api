import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../config';

export const generateToken = (payload: any) =>
  jwt.sign(payload, JWT_SECRET, { expiresIn: '30d' });

export const generatePayload: any = (token: string) =>
  jwt.verify(token, JWT_SECRET);
