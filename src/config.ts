import dotenv from 'dotenv';
import { Secret } from 'jsonwebtoken';
dotenv.config();

export const PORT: number = parseInt(process.env.PORT) || 3000;
export const JWT_SECRET: Secret = process.env.JWT_SECRET || '';
export const MONGODB_URL = process.env.MONGODB_URL || '';
export const REDIS_URL = process.env.REDIS_URL || '';
