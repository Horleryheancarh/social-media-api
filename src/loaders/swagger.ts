import swaggerJsdoc from 'swagger-jsdoc';

const options = {
  definition: {
    openapi: '3.0.1',
    info: {
      title: 'Social Media App API Documentation',
      version: '0.0.1',
      description: 'Social Media App API End Points Documentation',
    },
    servers: [
      {
        url: 'http://localhost:3000/api/v1/',
        description: 'Local server',
      },
      {
        url: 'https://#/',
        description: 'Live server',
      },
    ],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
    },
  },
  apis: ['src/routes/*.ts'],
};

const specs = swaggerJsdoc(options);

export { specs };