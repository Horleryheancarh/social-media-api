import express, { Application, Request, Response } from 'express';
import mongoose from 'mongoose';
const xss = require('xss-clean');
import rateLimit from 'express-rate-limit';
import helmet from 'helmet';
import cors from 'cors';
import swaggerUi from 'swagger-ui-express';
import errorHandler from '../middlewares/errorHandler';
import { specs } from './swagger';
import { MONGODB_URL } from '../config';
import AuthRouter from '../routes/auth.route';
import PostRouter from '../routes/post.route';
import authenticateUser from '../middlewares/authenticateUser';
import FollowRouter from '../routes/follow.route';

export default async (app: Application) => {
  app.use(rateLimit({
    windowMs: 15 * 60 * 1000,
    max: 60,
  }));
  app.use(helmet());
  app.use(cors());
  app.use(xss());

  app.use(express.json());

  // Database Connection
  mongoose.connect(MONGODB_URL)
    .then(() => console.log('Database connected'))
    .catch((err) => console.error(err));

  // routes
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
  app.use('/api/v1/auth', AuthRouter);
  app.use('/api/v1/posts', authenticateUser, PostRouter);
  app.use('/api/v1/follow', authenticateUser, FollowRouter);

  app.use((req: Request, res: Response, next) => {
    res.status(404).send('Route does not exist');
    next();
  });
  app.use(errorHandler);

  return app;
}