import { Router } from 'express';
import { paramValidationMiddleware } from '../validators';
import { followUser, getFollower, getFollowing, unFollowUser } from '../controllers/follow.controller';
import { IdSchema } from '../validators/post.validators';

/**
 * @swagger
 * tags:
 *   name: Follow
 */
const FollowRouter = Router()

/**
 * @swagger
 * /follow/followers:
 *   get:
 *     summary: Get Followers
 *     tags: [Follow]
 *     security:
 *       - apiKeyAuth: []
 *     responses:
 *       '200':
 *         description: Fetched Followers
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */
FollowRouter.get('/followers', getFollower);

/**
 * @swagger
 * /follow/followers:
 *   get:
 *     summary: Get Following
 *     tags: [Follow]
 *     security:
 *       - apiKeyAuth: []
 *     responses:
 *       '200':
 *         description: Fetched Followings
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */
FollowRouter.get('/followings', getFollowing);

/**
 * @swagger
 * /follow/follow/:id:
 *   patch:
 *     summary: Follow User
 *     tags: [Follow]
 *     security:
 *       - apiKeyAuth: []
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user to follow
 *         schema:
 *           type: string
 *     responses:
 *       '204':
 *         description: Followed User
 */
FollowRouter.patch('/follow/:id', paramValidationMiddleware(IdSchema), followUser);

/**
 * @swagger
 * /follow/unfollow/:id:
 *   patch:
 *     summary: Unfollow user
 *     tags: [Follow]
 *     security:
 *       - apiKeyAuth: []
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user to follow
 *         schema:
 *           type: string
 *     responses:
 *       '204':
 *         description: Unfollowed User
 */
FollowRouter.patch('/unfollow/:id', paramValidationMiddleware(IdSchema), unFollowUser);


export default FollowRouter;
