import { Router } from 'express';
import { signIn, signUp } from '../controllers/auth.controller';
import { bodyValidationMiddleware } from '../validators';
import { UserCreateSchema, UserLoginSchema } from '../validators/auth.validators';

/**
 * @swagger
 * tags:
 *   name: Auth
 */
const AuthRouter = Router()

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         firstname:
 *           type: string
 *           description: The first name of the user
 *         lastname:
 *           type: string
 *           description: The last name of the user
 *         middlename:
 *           type: string
 *           description: The middle name of the user
 *         username:
 *           type: string
 *           description: Unique name of the user
 *         email:
 *           type: string
 *           description: User's mail
 *         phone:
 *           type: string
 *           description: User's phone
 *         password:
 *           type: string
 *           format: password
 *           description: password
 *     UserResponse:
 *       type: object
 *       properties:
 *         message:
 *           type: string
 *         token:
 *           type: string
 *         user:
 *           type: object
 *           properties:
 *             firstname:
 *               type: string
 *               description: The first name of the user
 *             lastname:
 *               type: string
 *               description: The last name of the user
 *             middlename:
 *               type: string
 *               description: The middle name of the user
 *             username:
 *               type: string
 *               description: Unique name of the user
 *             email:
 *               type: string
 *               description: User's mail
 *             phone:
 *               type: string
 *               description: User's phone
 */

/**
 * @swagger
 * /auth/sign-up/:
 *   post:
 *     summary: Create new User
 *     tags: [Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User'
 *     responses:
 *       '201':
 *         description: Author created
 *         content:
 *           application/json:
 *             schema:
 *                 $ref: '#/components/schemas/UserResponse'
 *       '401':
 *         description: Unauthorized Error
 *         content:
 *           application/json:
 *             message: 'Email or Phone already in use'
 */
AuthRouter.post('/sign-up', bodyValidationMiddleware(UserCreateSchema), signUp);

/**
 * @swagger
 * /auth/sign-in/:
 *   post:
 *     summary: Sign In User
 *     tags: [Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           identifier:
 *             type: string
 *             description: Sign In Successful
 *           password:
 *             type: string
 *             format: password
 *             description: password
 *     responses:
 *       '200':
 *         description: Sign In succesful
 *         content:
 *           application/json:
 *             schema:
 *                 $ref: '#/components/schemas/UserResponse'
 *       '401':
 *         description: Unauthorized Error
 *         content:
 *           application/json:
 *             message: 'Invalid Login Details'
 */
AuthRouter.post('/sign-in', bodyValidationMiddleware(UserLoginSchema), signIn);

export default AuthRouter;
