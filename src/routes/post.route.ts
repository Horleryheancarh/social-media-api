import { Router } from 'express';
import { IdSchema, PostCreateSchema, PostUpdateSchema } from '../validators/post.validators';
import { createPost, deletePost, getAllPosts, getPostsFeed, getSinglePost, likePost, unLikePost, updatePost } from '../controllers/post.controller';
import { bodyValidationMiddleware, paramValidationMiddleware } from '../validators';
import { CommentCreateSchema, CommentUpdateSchema, CommentIdSchema, PostIdSchema } from '../validators/comment.validators';
import { createComment, deleteComment, getAllPostComments, getSingleComment, likeComment, unLikeComment, updateComment } from '../controllers/comment.controller';
import { PaginationSchema } from '../validators/pagination';

/**
 * @swagger
 * tags:
 *   name: Post
 */
const PostRouter = Router()

/**
 * @swagger
 * components:
 *   schemas:
 *     Post:
 *       type: object
 *       properties:
 *         content:
 *           type: string
 *           description: Post Content
 *         imageUrl:
 *           type: string
 *           description: Post Image Url
 *     PostResponse:
 *       type: object
 *       properties:
 *         message:
 *           type: string
 *         post:
 *           type: object
 *           properties:
 *             author:
 *               type: string
 *               description: author
 *             content:
 *               type: string
 *               description: Post Content
 *             imageUrl:
 *               type: string
 *               description: Post Image Url
 *             tags:
 *               type: string[]
 *               description: Tags in the post
 *             likes:
 *               type: string[]
 *               description: List of Users who liked the post
 *     Pagination:
 *       type: object
 *       properties:
 *         - in: query
 *           page: 
 *             type: number
 *         - in: query
 *           limit:
 *             type: number
 */

/**
 * @swagger
 * /posts:
 *   post:
 *     summary: Create new Post
 *     tags: [Post]
 *     security:
 *       - apiKeyAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Post'
 *     responses:
 *       '201':
 *         description: Post created
 *         content:
 *           application/json:
 *             schema:
 *                 $ref: '#/components/schemas/PostResponse'
 *       '401':
 *         description: Unauthorized Error
 *   get:
 *     summary: Get All Posts
 *     tags: [Post]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: query
 *         name: pagination
 *         schema:
 *           $ref: '#/components/schemas/Pagination'
 *     responses:
 *       '200':
 *         description: Fetched Posts
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/PostResponse'
 */
PostRouter.post(
  '/',
  bodyValidationMiddleware(PostCreateSchema),
  createPost
);
PostRouter.get(
  '/',
  paramValidationMiddleware(PaginationSchema),
  getAllPosts
);
/**
 * @swagger
 * /posts/feed:
 *   get:
 *     summary: Get My Feed Posts
 *     tags: [Post]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: query
 *         name: pagination
 *         schema:
 *           $ref: '#/components/schemas/Pagination'
 *     responses:
 *       '200':
 *         description: Fetched Posts
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/PostResponse'
*/
PostRouter.get(
  '/feed',
  paramValidationMiddleware(PaginationSchema),
  getPostsFeed
);

/**
 * @swagger
 * /posts/:id:
 *   get:
 *     summary: Fetch Post
 *     tags: [Post]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the post to retrieve
 *         schema:
 *           type: string
 *     responses:
 *       '201':
 *         description: Post created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/PostResponse'
 *       '401':
 *         description: Unauthorized Error
 *   put:
 *     summary: Get All Posts
 *     tags: [Post]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the post to retrieve
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Post'
 *     responses:
 *       '200':
 *         description: Updated Post
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/PostResponse'
 *   delete:
 *     summary: Fetch Post
 *     tags: [Post]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the post to retrieve
 *         schema:
 *           type: string
 *     responses:
 *       '204':
 *         description: Post deleted
 *       '401':
 *         description: Unauthorized Error
 */
PostRouter.get('/:id',
  paramValidationMiddleware(IdSchema),
  getSinglePost
);
PostRouter.put('/:id',
paramValidationMiddleware(IdSchema),
  bodyValidationMiddleware(PostUpdateSchema),
  updatePost
);
PostRouter.delete('/:id', paramValidationMiddleware(IdSchema), deletePost);

/**
 * @swagger
 * /posts/:id/like:
 *   patch:
 *     summary: Like Post
 *     tags: [Post]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the post to retrieve
 *         schema:
 *           type: string
 *     responses:
 *       '204':
 *         description: Post deleted
 *       '401':
 *         description: Unauthorized Error
 * /posts/:id/unlike:
 *   patch:
 *     summary: Unlike Post
 *     tags: [Post]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the post to retrieve
 *         schema:
 *           type: string
 *     responses:
 *       '204':
 *         description: Post deleted
 *       '401':
 *         description: Unauthorized Error
 */
PostRouter.patch('/:id/like', paramValidationMiddleware(IdSchema), likePost);
PostRouter.patch('/:id/unlike', paramValidationMiddleware(IdSchema), unLikePost);

/**
 * @swagger
 * components:
 *   schemas:
 *     Comment:
 *       type: object
 *       properties:
 *         content:
 *           type: string
 *           description: Comment Content
 *         imageUrl:
 *           type: string
 *           description: Comment Image Url
 *     CommentResponse:
 *       type: object
 *       properties:
 *         message:
 *           type: string
 *         Comment:
 *           type: object
 *           properties:
 *             author:
 *               type: string
 *               description: author
 *             content:
 *               type: string
 *               description: Comment Content
 *             imageUrl:
 *               type: string
 *               description: Comment Image Url
 *             tags:
 *               type: string[]
 *               description: Tags in the Comment
 *             likes:
 *               type: string[]
 *               description: List of Users who liked the Comment
 */

/**
 * @swagger
 * /posts/:postId/comments:
 *   post:
 *     summary: Create new Comment
 *     tags: [Comment]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: postId
 *         required: true
 *         description: ID of the post to retrieve
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Comment'
 *     responses:
 *       '201':
 *         description: Comment created
 *         content:
 *           application/json:
 *             schema:
 *                 $ref: '#/components/schemas/CommentResponse'
 *       '401':
 *         description: Unauthorized Error
 *   get:
 *     summary: Get All Comments
 *     tags: [Comment]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: query
 *         name: pagination
 *         schema:
 *           $ref: '#/components/schemas/Pagination'
 *       - in: path
 *         name: postId
 *         required: true
 *         description: ID of the post to retrieve
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Fetched Comments
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/CommentResponse'
 */
PostRouter.post(
  '/:postId/comments',
  bodyValidationMiddleware(CommentCreateSchema),
  createComment
);
PostRouter.get(
  '/:postId/comments',
  paramValidationMiddleware(PostIdSchema.merge(PaginationSchema)),
  getAllPostComments
);

/**
 * @swagger
 * /posts/:postId/comments/:commentId:
 *   get:
 *     summary: Fetch Comment
 *     tags: [Comment]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: postId
 *         required: true
 *         description: ID of the post
 *         schema:
 *           type: string
 *       - in: path
 *         name: commentId
 *         required: true
 *         description: ID of the comment
 *         schema:
 *           type: string
 *     responses:
 *       '201':
 *         description: Comment created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CommentResponse'
 *       '401':
 *         description: Unauthorized Error
 *   put:
 *     summary: Get All Comments
 *     tags: [Comment]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: postId
 *         required: true
 *         description: ID of the post
 *         schema:
 *           type: string
 *       - in: path
 *         name: commentId
 *         required: true
 *         description: ID of the comment
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Comment'
 *     responses:
 *       '200':
 *         description: Updated Comment
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CommentResponse'
 *   delete:
 *     summary: Fetch Comment
 *     tags: [Comment]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: postId
 *         required: true
 *         description: ID of the post
 *         schema:
 *           type: string
 *       - in: path
 *         name: commentId
 *         required: true
 *         description: ID of the comment
 *         schema:
 *           type: string
 *     responses:
 *       '204':
 *         description: Comment deleted
 *       '401':
 *         description: Unauthorized Error
 */
PostRouter.get(
  '/:postId/comments/:commentId',
  paramValidationMiddleware(PostIdSchema.merge(CommentIdSchema)),
  getSingleComment
);
PostRouter.put(
  '/:postId/comments/:commentId',
  paramValidationMiddleware(PostIdSchema.merge(CommentIdSchema)),
  bodyValidationMiddleware(CommentUpdateSchema),
  updateComment
);
PostRouter.delete(
  '/:postId/comments/:commentId',
  paramValidationMiddleware(PostIdSchema.merge(CommentIdSchema)),
  deleteComment
);

/**
 * @swagger
 * /posts/:postId/comments/:commentId/like:
 *   patch:
 *     summary: Like Comment
 *     tags: [Comment]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: postId
 *         required: true
 *         description: ID of the post
 *         schema:
 *           type: string
 *       - in: path
 *         name: commentId
 *         required: true
 *         description: ID of the comment
 *         schema:
 *           type: string
 *     responses:
 *       '204':
 *         description: Comment deleted
 *       '401':
 *         description: Unauthorized Error
 * /posts/:postId/comments/:commentId/unlike:
 *   patch:
 *     summary: Unlike Comment
 *     tags: [Comment]
 *     security:
 *       - apiKeyAuth: []
 *     parameters:
 *       - in: path
 *         name: postId
 *         required: true
 *         description: ID of the post
 *         schema:
 *           type: string
 *       - in: path
 *         name: commentId
 *         required: true
 *         description: ID of the comment
 *         schema:
 *           type: string
 *     responses:
 *       '204':
 *         description: Comment deleted
 *       '401':
 *         description: Unauthorized Error
 */
PostRouter.patch(
  '/:postId/comments/:commentId/like',
  paramValidationMiddleware(PostIdSchema.merge(CommentIdSchema)),
  likeComment
);
PostRouter.patch(
  '/:postId/comments/:commentId/unlike',
  paramValidationMiddleware(PostIdSchema.merge(CommentIdSchema)),
  unLikeComment
);

export default PostRouter;