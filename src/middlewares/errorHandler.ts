import { Request, Response, NextFunction } from 'express';

// more error handling will be done here. This is just the bare minimum
export default function errorHandler(
  err: any,
  req: Request,
  res: Response,
  next: NextFunction
) {
  const errorToHandle = {
    message: err.message || 'Something went wrong. Please try again later',
    statusCode: err.statusCode || 500,
  };

  res.status(errorToHandle.statusCode).json({ msg: errorToHandle.message });
}
