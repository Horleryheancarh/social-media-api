import { Response, NextFunction } from 'express';
import { Req } from '../interfaces';
import { UnauthorizedError, generatePayload } from '../utils';

const authenticateUser = (req: Req, res: Response, next: NextFunction) => {
  if (
    !req.headers.authorization ||
    !req.headers.authorization.startsWith('Bearer ')
  )
    throw new UnauthorizedError('Authorization failed. Login to continue');
  const token = req.headers.authorization.split(' ')[1];
  try {
    const payload = generatePayload(token);
    req.user = payload;
    next();
  } catch (error) {
    next(error);
  }
};

export default authenticateUser;