import { NextFunction, Request, RequestHandler, Response } from 'express';
import { Req } from '../interfaces';
import { CommentService } from '../services/comment.service';

export const createComment: RequestHandler = async (
  req: Req,
  res: Response,
  next: NextFunction,
) => {
  try {
    const comment = await CommentService.create({ ...req.body, author: req.user.id });

    res.status(201).json({
      message: 'Comment created',
      comment,
    })
  } catch (error) {
    next(error)
  }
}

export const getAllPostComments: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const comments = await CommentService.findPostComments(req.params.postId, req.params.page, req.params.limit);

    res.status(200).json({
      message: 'Fetched comments',
      comments,
    })
  } catch (error) {
    next(error)
  }
}

export const getSingleComment: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const comment = await CommentService.findPostComments(req.params.commentId);

    res.status(200).json({
      message: 'Fetched comment',
      comment,
    })
  } catch (error) {
    next(error)
  }
}

export const updateComment: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const comments = await CommentService.updateComment(req.params.commentId, {
      ...req.body,
    });

    res.status(200).json({
      message: 'Fetched comments',
      comments,
    })
  } catch (error) {
    next(error)
  }
}

export const deleteComment: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    await CommentService.deleteComment(req.params.commentId);

    res.status(204)
  } catch (error) {
    next(error)
  }
}

export const likeComment: RequestHandler = async (
  req: Req,
  res: Response,
  next: NextFunction
) => {
  try {
    await CommentService.likeComment(req.params.id, req.user.id);

    res.status(204)
  } catch (error) {
    next(error);
  }
}

export const unLikeComment: RequestHandler = async (
  req: Req,
  res: Response,
  next: NextFunction
) => {
  try {
    await CommentService.unlikeComment(req.params.id, req.user.id);

    res.status(204)
  } catch (error) {
    next(error);
  }
}