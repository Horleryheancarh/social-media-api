import { NextFunction, Request, RequestHandler, Response } from 'express';
import { AuthService } from '../services/auth.service';

export const signUp: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { user, token } = await AuthService.signup(req.body);

    res.status(201).json({
      message: 'Account created',
      user,
      token,
    })
  } catch (error) {
    next(error);
  }
}

export const signIn: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { user, token } = await AuthService.signin(req.body);

    res.status(200).json({
      message: 'Sign In Successful',
      user,
      token,
    })
  } catch (error) {
    next(error);
  }
}
