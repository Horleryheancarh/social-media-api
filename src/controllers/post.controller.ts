import { NextFunction, Request, RequestHandler, Response } from 'express';
import { PostService } from '../services/post.service';
import { Req } from '../interfaces';

export const createPost: RequestHandler = async (
  req: Req,
  res: Response,
  next: NextFunction
) => {
   try {
     const post = await PostService.create({ ...req.body, author: req.user.id });

     res.status(201).json({
      message: 'Post created',
      post,
     })
   } catch (error) {
     next(error);
   }
}

export const getSinglePost: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const post = await PostService.findOne(req.params.id);

    res.status(200).json({
      message: 'Fetched Post',
      post
    })
  } catch (error) {
    next(error);
  }
}

export const getPostsFeed: RequestHandler = async (
  req: Req,
  res: Response,
  next: NextFunction
) => {
  try {
    const posts = await PostService.getFeed(req.user.id, parseInt(req.params.page), parseInt(req.params.limit));

    res.status(200).json({
      message: 'Fetched All Posts',
      posts
    })
  } catch (error) {
    next(error);
  }
}

export const getAllPosts: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const posts = await PostService.findAll(parseInt(req.params.page), parseInt(req.params.limit));

    res.status(200).json({
      message: 'Fetched All Posts',
      posts
    })
  } catch (error) {
    next(error);
  }
}

export const updatePost: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const post = await PostService.update(req.params.id, req.body);

    res.status(200).json({
      message: 'Updated Post',
      post
    })
  } catch (error) {
    next(error);
  }
}

export const deletePost: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    await PostService.delete(req.params.id);

    res.status(204)
  } catch (error) {
    next(error);
  }
}

export const likePost: RequestHandler = async (
  req: Req,
  res: Response,
  next: NextFunction
) => {
  try {
    await PostService.likePost(req.params.id, req.user.id);

    res.status(204)
  } catch (error) {
    next(error);
  }
}

export const unLikePost: RequestHandler = async (
  req: Req,
  res: Response,
  next: NextFunction
) => {
  try {
    await PostService.unlikePost(req.params.id, req.user.id);

    res.status(204)
  } catch (error) {
    next(error);
  }
}