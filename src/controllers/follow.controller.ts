import { NextFunction, RequestHandler, Response } from 'express';
import { Req } from '../interfaces';
import { FollowService } from '../services/follow.service';

export const followUser: RequestHandler = async(
  req: Req,
  res: Response,
  next: NextFunction
) => {
  try {
    await FollowService.follow(req.user.id, req.params.followId)

    res.status(204)
  } catch (error) {
    next(error)
  }
}

export const unFollowUser: RequestHandler = async(
  req: Req,
  res: Response,
  next: NextFunction
) => {
  try {
    await FollowService.unfollow(req.user.id, req.params.followId)

    res.status(204)
  } catch (error) {
    next(error)
  }
}

export const getFollower: RequestHandler = async(
  req: Req,
  res: Response,
  next: NextFunction
) => {
  try {
    const followers = await FollowService.getFollowers(req.user.id)

    res.status(200).json({
      message: 'Fetched followers',
      followers
    })
  } catch (error) {
    next(error)
  }
}

export const getFollowing: RequestHandler = async(
  req: Req,
  res: Response,
  next: NextFunction
) => {
  try {
    const followings = await FollowService.getFollowings(req.user.id)

    res.status(200).json({
      message: 'Fetched followings',
      followings
    })
  } catch (error) {
    next(error)
  }
}
