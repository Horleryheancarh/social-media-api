import mongoose, { Schema, model } from 'mongoose';

const postSchema = new Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  content: {
    type: String,
  },
  imageUrl: {
    type: String,
  },
  tags: {
    type: [String],
  },
  likes: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  }]
})

export interface postInterface {
  author: string,
  content: string,
  imageUrl: string,
  likes: string[],
}

export const Post = model('post', postSchema);
