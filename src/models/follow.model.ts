import mongoose, { Schema, model } from 'mongoose';

const followSchema = new Schema({
  following: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  follower: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }
})

export const Follow = model('follow', followSchema);
