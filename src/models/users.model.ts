import { Schema, model } from 'mongoose';

const userSchema = new Schema({
  firstname: {
    type: String,
  },
  lastname: {
    type: String,
  },
  middlename: {
    type: String,
  },
  username: {
    type: String,
    required: true,
    unique: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  phone: {
    type: String,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  }
})

export interface userInterface {
  firstname: string,
  lastname: string,
  middlename: string,
  username: string,
  email: string,
  phone: string,
  password: string,
}

export const User = model('user', userSchema);