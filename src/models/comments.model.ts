import mongoose, { Schema, model } from 'mongoose';

const commentSchema = new Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  post: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'post'
  },
  content: {
    type: String,
  },
  imageUrl: {
    type: String,
  },
  tags: {
    type: [String],
  },
  likes: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  }]
})

export interface commentInterface {
  author: string,
  post: string,
  content: string,
  imageUrl: string,
  likes: string[],
}

export const Comment = model('comment', commentSchema);
