import express from 'express';
import "express-async-errors";
import loaders from './loaders';
import { PORT } from './config';

async function startServer() {
  const app = express();

  await loaders(app);

  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
}

startServer();
