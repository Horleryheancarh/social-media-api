import { compare, genSalt, hash } from 'bcryptjs';
import { User, userInterface } from '../models/users.model';
import { UnauthorizedError, generateToken } from '../utils';

export class AuthService {
  static async signup(data: userInterface) {
    const { firstname, lastname, middlename, username, email, phone, password } = data;

    if (await User.findOne({ $or: [{ email }, { phone }, { username }] }))
      throw new UnauthorizedError('Email, Phone or Username already in use');
    
    const salt = await genSalt();
    const hashedPassword = await hash(password, salt);
    const newUser = await User.create({
      firstname,
      lastname,
      middlename,
      username,
      email,
      phone,
      password: hashedPassword,
    });

    const user = newUser.toObject();
    delete user.password;


    return {
      token: generateToken({ id: user._id, email: user.email, phone: user.phone }),
      user
    }
  }

  static async signin(data: {
    identifier: string,
    password: string,
  }) {
    const { identifier, password } = data;
    const foundUser = await User.findOne({ $or: [{ email: identifier }, { phone: identifier }, { username: identifier }] })

    if (!foundUser) throw new UnauthorizedError('Invalid Login Details');

    if (await compare(foundUser.password, password)) throw new UnauthorizedError('Invalid Login Details');

    const user = foundUser.toObject();
    delete user.password;


    return {
      token: generateToken({ id: user._id, email: user.email, phone: user.phone }),
      user
    }
  }
}