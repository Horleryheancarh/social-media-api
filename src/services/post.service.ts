import { Comment } from '../models/comments.model';
import { Follow } from '../models/follow.model';
import { Post, postInterface } from '../models/posts.model';
import { NotFoundError } from '../utils';
import { delCache, getCache, setCache } from '../utils/cache.util';

export class PostService {
  static async create(data: postInterface) {
    const { content, imageUrl, author } = data;
    const tags = content.match(/#\w+/g);
    const mentions = content.match(/@\w+/g);

    const post = await Post.create({
      content, imageUrl, author, tags
    })

    /** TODO
     * Emit event for mentions notifications
     */

    return post;
  }

  static async findAll(page: number, limit: number = 20) {
    const skip = (page - 1) * limit;
    const totalPosts = await Post.countDocuments();
    const totalPages = Math.ceil(totalPosts / limit);
    const posts = await Post.find()
      .sort({ createdAt: 1 })
      .skip(skip)
      .limit(limit);

    return {
      posts,
      totalPosts,
      totalPages,
      page,
      limit
    };
  }

  static async getFeed(userId: string, page: number, limit: number = 20) {
    const skip = (page - 1) * limit;
    const following = await Follow.find({ follower: userId })
    const authors = following.map((data) => data.following);
    
    const totalPosts = await Post.countDocuments({ author: { $in: [userId, ...authors] } });
    const totalPages = Math.ceil(totalPosts / limit);

    const posts = await Post.find({ author: { $in: [userId, ...authors] } })
      .sort({ createdAt: 1 })
      .skip(skip)
      .limit(limit);

    return {
      posts,
      totalPosts,
      totalPages,
      page,
      limit
    };
  }

  static async findOne(id: string) {
    const cachedPost = await getCache(id);
    if (cachedPost) return cachedPost;

    const post = await Post.findById(id);
    if (!post) throw new NotFoundError('Post not Found');
    const numOfComments = await Comment.countDocuments({ postId: id });

    await setCache({ key: id, ttl: 600, value: { post, numOfComments } })

    return {
      post,
      numOfComments
    };
  }

  static async update(id: string, data: postInterface) {
    const cachedPost = await getCache(id);
    if (cachedPost) await delCache(id);

    const post = await Post.findByIdAndUpdate(id, { ...data });

    return post;
  }

  static async delete(id: string) {
    await Post.findByIdAndDelete(id);
  }

  static async likePost(id: string, userId: string) {
    await Post.findByIdAndUpdate(id, { $addToSet: { likes: userId } });
  }

  static async unlikePost(id: string, userId: string) {
    await Post.findByIdAndUpdate(id, { $pull: { likes: userId } });
  }
}