import { Comment, commentInterface } from '../models/comments.model';
import { UnauthorizedError } from '../utils';

export class CommentService {
  static async create(data: commentInterface) {
    const { post, content, imageUrl, author } = data;
    const tags = content.match(/#\w+/g);
    const mentions = content.match(/@\w+/g);

    const comment = await Comment.create({
      post, imageUrl, author, content, tags
    })
    /** TODO
     * Emit event for mentions notifications
     */

    return comment;
  }

  static async findPostComments(post: string, page: number, limit: number) {
    const skip = (page - 1) * limit;
    const totalComments = await Comment.countDocuments({ post });
    const totalPages = Math.ceil(totalComments / limit);
    const comments = await Comment.find({ post })
    .sort({ createdAt: 1 })
    .skip(skip)
    .limit(limit);

    return {
      comments,
      totalComments,
      totalPages,
      page,
      limit
    };
  }

  static async findSingleComment(commentId: string) {
    const comment = await Comment.findById(commentId);

    return comment;
  }

  static async updateComment(commentId: string, data: commentInterface) {
    const comment = await Comment.findByIdAndUpdate(commentId, { ...data });

    return comment;
  }

  static async deleteComment(id: string) {
    await Comment.findByIdAndDelete(id);
  }

  static async likeComment(id: string, userId: string) {
    await Comment.findByIdAndUpdate(id, { a$ddToSet: { likes: userId } });
  }

  static async unlikeComment(id: string, userId: string) {
    await Comment.findByIdAndUpdate(id, { $pull: { likes: userId } });
  }
}