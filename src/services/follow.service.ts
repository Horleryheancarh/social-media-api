import { Follow } from '../models/follow.model';
import { BadRequestError, NotAcceptableError } from '../utils';

export class FollowService {
  static async follow(userId: string, followId: string) {
    if (userId == followId) throw new BadRequestError('Can not follow yourself');

    if (await Follow.findOne({ following: userId, follower: followId }))
      throw new NotAcceptableError('Already following')

    await Follow.create({ following: userId, follower: followId })
  }

  static async unfollow(userId: string, followId: string) {
    await Follow.deleteOne({ following: userId, follower: followId })
  }

  static async followStats(userId: string) {
    const follower = await Follow.countDocuments({ following: userId });
    const following = await Follow.countDocuments({ follower: userId });

    return {
      follower,
      following
    };
  }

  static async getFollowers(userId: string) {
    return await Follow.find({ following: userId })
      .populate({ path: 'follower', select: '-password' });
  }

  static async getFollowings(userId: string) {
    return await Follow.find({ follower: userId })
      .populate({ path: 'following', select: '-password' });;
  }
}