# Social Media Api

## Description
A RESTFUL API for a basic social media platform.

## Installation
```bash
# Install dependencies
yarn install

```

## Usage
```bash
# Run in dev mode
yarn start:dev

# Build
yarn build

# Run in production
yarn start
```

## Documentation
Swagger Documentation Url is /api-docs

## Author
@Horleryheancarh


## Project status
In Progress
